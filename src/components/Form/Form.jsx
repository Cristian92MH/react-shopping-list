import React, { Fragment } from 'react';
import Estimate from '../Estimate/Estimate';
import './Form.css';
import shortid from 'shortid';
import List from '../List/List';
const Form = () => {
    const [ product, setProduct ] = React.useState();
    const [ price, setPrice ] = React.useState();
    const [ productInfo, setProductInfo ] = React.useState([]);
    const [ id, setId ] = React.useState('');
    const [ resultAmount, setResultAmount ] = React.useState(0);
    
    const [ calculateAmount, setCalculateAmount ] = React.useState();
    console.log(calculateAmount)
    const saveProduct = (e) => {
        e.preventDefault();
        setProductInfo([ ...productInfo, {
            'id': shortid.generate(),
            'product': product,
            'price': price
        }]);
        console.log(price)
        setResultAmount(calculateAmount - price);
        setCalculateAmount(calculateAmount - price);
        setId('');
        setProduct('');
        //setPrice('');
        e.target.reset();
    }
    return (
        <Fragment>
            <Estimate result={ resultAmount } amountRemaining={ setCalculateAmount }/>
            <h1>Formulario</h1>
            <form onSubmit = { saveProduct }>
                <label htmlFor="product_name">Nombre del producto: </label>
                <input 
                    type="text" 
                    id="product_name" 
                    name="product" 
                    placeholder="Producto" 
                    onChange={ (e) => (setProduct(e.target.value))}
                />
                <label htmlFor="product_price">Precio del Producto</label>
                <input 
                    type="number"
                    id="product_price"
                    name="price"
                    placeholder="Precio"
                    onChange={ (e) => (setPrice(e.target.value))}
                />
                <button type="submit">Guardar</button>
            </form>
            <List  dataList={ productInfo }/>
        </Fragment>
    );
}
export default Form;