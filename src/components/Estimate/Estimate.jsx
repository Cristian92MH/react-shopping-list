import React, { Fragment } from 'react';
import './Estimate.css';
const Estimate = ( { amountRemaining, result }) => {
    const [ estimatedAmount, setEstimatedAmount ] = React.useState();
    const [ saveAmount, setSaveAmount ] = React.useState(0);
    console.log('result', result);
    const saveEstimated = (e) => {
        e.preventDefault();
        setSaveAmount(estimatedAmount);
        amountRemaining(estimatedAmount);
        e.target.reset();
    }
    return (
        <Fragment>
            <h1>Estimado</h1>
            <form onSubmit={ saveEstimated }>
                <label htmlFor="">Ingresa tu monto estimado a gastar: </label>
                <input 
                    type="number" 
                    name="estimated"
                    placeholder="Monto estimado"
                    onChange={ (e)=>( setEstimatedAmount(e.target.value)) }
                />
                <button type="submit">Guardar</button>
            </form>
            <span>Tu monto estimado: { saveAmount }</span>
    <span> Monto restante: { result } </span>
        </Fragment>
    );
}
export default Estimate;