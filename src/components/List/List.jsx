import React from 'react';
import { Fragment } from 'react';

const List = ( { dataList }) => {
    console.log(dataList);
    return (
        <Fragment>
            <h1>Lista</h1>
            <span>
                {
                    dataList.map( item =>
                    <span key={ item.id }> Producto: { item.product}  - Precio: $ { item.price }</span>
                        )
                }
            </span>
        </Fragment>
    );
}
export default List;